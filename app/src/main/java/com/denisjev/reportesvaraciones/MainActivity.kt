package com.denisjev.reportesvaraciones

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.concurrent.ListenableFuture
import com.esri.arcgisruntime.data.*
import com.esri.arcgisruntime.layers.FeatureLayer

class MainActivity : AppCompatActivity() {
    private var mRequestQueue: RequestQueue? = null
    private var instaModalArrayList: ArrayList<ReportedFeedModal>? = null
    private var reportsFeedModalArrayList: ArrayList<ReportedFeedModal>? = null
    private var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_element_layout)
        progressBar = findViewById(R.id.idLoadingPB)

        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            val intent = Intent(this, PostData::class.java)
            startActivity(intent)
        }

        ArcGISRuntimeEnvironment.setApiKey(resources.getString(R.string.API))
        val serviceFeatureTable = ServiceFeatureTable(resources.getString(R.string.service_url))
        val featureLayer = FeatureLayer(serviceFeatureTable)
        val query = QueryParameters()
        query.whereClause = ("1=1")
        val future: ListenableFuture<FeatureQueryResult> =
            serviceFeatureTable.queryFeaturesAsync(
                query,
                ServiceFeatureTable.QueryFeatureFields.LOAD_ALL
            )
        future.addDoneListener {
            try {
                val result = future.get()
                progressBar!!.visibility = View.GONE

                reportsFeedModalArrayList = ArrayList<ReportedFeedModal>()

                result.forEach {
                    val attrs = it.attributes

                    val authorImage = "https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-512.png"
                    val authorName = attrs["nombre"].toString()
                    val postDate = "" //feedsObj.getString("postDate")
                    val postDescription = attrs["descripcion"].toString()
                    val postIV ="https://static.abc.es/media/sociedad/2019/06/06/tortuga-mora-U30833967479SdH--620x349@abc.jpg" //feedsObj.getString("postIV")
                    var postFeature = it as ArcGISFeature
                    val postLikes = "" //feedsObj.getString("postLikes")
                    val postComments = "" //feedsObj.getString("postComments")

                    val feedModal = ReportedFeedModal(
                        authorImage,
                        authorName,
                        postDate,
                        postDescription,
                        postIV,
                        postFeature,
                        postLikes,
                        postComments
                    )
                    reportsFeedModalArrayList!!.add(feedModal)

                    var adapter = ReportedFeedRVAdapter(reportsFeedModalArrayList!!,this@MainActivity)
                    var instRV = findViewById<RecyclerView>(R.id.idRVInstaFeeds)

                    val linearLayoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
                    instRV.layoutManager = linearLayoutManager
                    instRV.adapter = adapter
                }
            } catch (e: Exception) {
                "Feature search failed ".also {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    Log.e("ERROR", e.message.toString())
                }
            }
        }
        //reportedFeeds
    }
}