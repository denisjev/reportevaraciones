package com.denisjev.reportesvaraciones

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.denisjev.reportesvaraciones.databinding.PostDataBinding
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.ArcGISRuntimeException
import com.esri.arcgisruntime.concurrent.ListenableFuture
import com.esri.arcgisruntime.data.*
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.layers.FeatureLayer
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.BasemapStyle
import com.esri.arcgisruntime.mapping.Viewpoint
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener
import com.esri.arcgisruntime.mapping.view.MapView
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream


//import com.google.android.gms.location.FusedLocationProviderClient
//import com.google.android.gms.location.LocationServices

class PostData : AppCompatActivity() {

    private val RESULT_LOAD_IMAGE = 1
    private var selectedImage: Uri? = null
    private var mSelectedArcGISFeature: ArcGISFeature? = null
    private var mServiceFeatureTable : ServiceFeatureTable? = null

    //private lateinit var fusedLocationClient: FusedLocationProviderClient
    val getContent = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { result ->
        if(result.resultCode == Activity.RESULT_OK) {
            uploadAttachment(result)
        }
    })

    private val postDataBinding by lazy {
        PostDataBinding.inflate(layoutInflater)
    }
    private val mapView: MapView by lazy {
        postDataBinding.mapView
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(postDataBinding.root)

        //mServiceFeatureTable = ServiceFeatureTable(resources.getString(com.denisjev.reportesvaraciones.R.string.service_url))

        ArcGISRuntimeEnvironment.setApiKey(resources.getString(R.string.API))
        // create a map with streets basemap
        ArcGISMap(BasemapStyle.ARCGIS_STREETS).let { map ->
            // create and load the service geodatabase
            ServiceGeodatabase(getString(R.string.service_url)).apply {
                loadAsync()
                addDoneLoadingListener {
                    // create a feature layer using the first layer in the geodatabase
                    mServiceFeatureTable = getTable(0)
                    // add a listener to the MapView to detect when a user has performed a single tap to add a new feature to
                    // the service feature table
                    mapView.onTouchListener =
                        object : DefaultMapViewOnTouchListener(this@PostData, mapView) {

                            override fun onSingleTapConfirmed(motionEvent: MotionEvent?): Boolean {
                                motionEvent?.let { event ->
                                    // create a point from where the user clicked
                                    android.graphics.Point(event.x.toInt(), event.y.toInt())
                                        .let { point ->
                                            // create a map point from a point
                                            mapView.screenToLocation(point)
                                        }.let { mapPoint ->
                                            // add a new feature to the service feature table
                                            Log.i("ERROR: ", mapPoint.toJson())
                                            addFeature(mapPoint, mServiceFeatureTable!!)
                                        }
                                }
                                return super.onSingleTapConfirmed(motionEvent)
                            }
                        }
                    // create a feature layer from table
                    val featureLayer = FeatureLayer(mServiceFeatureTable)
                    // add the layer to the map
                    map.operationalLayers.add(featureLayer)
                }
            }
            // set map to be displayed in map view
            mapView.map = map

            // set an initial view point
            mapView.setViewpoint(Viewpoint(12.431565, -86.872205, 1000000.0))
        }
    }

    private fun addFeature(mapPoint: Point, featureTable: ServiceFeatureTable) {
        // create default attributes for the feature
        hashMapOf<String, Any>(
            "nombre" to "Destroyed",
            "descripcion" to "Earthquake"
        ).let { attributes ->
            // creates a new feature using default attributes and point
            featureTable.createFeature(attributes, mapPoint)
        }.let { feature ->
            // check if feature can be added to feature table
            if (featureTable.canAdd()) {
                // add the new feature to the feature table and to server
                featureTable.addFeatureAsync(feature).addDoneListener {
                    mSelectedArcGISFeature = feature as ArcGISFeature?
                    selectAttachment()
                    //applyEdits()
                    //Log.i("INFO: ", "Dato agregado")
                }
            } else {
                logToUser(true, "No se puede agregar la feature")
            }
        }

    }

    private fun applyEdits() {

        // apply the changes to the server
        mServiceFeatureTable!!.applyEditsAsync().let { editResult ->
            editResult.addDoneListener {
                try {
                    editResult.get()?.let { edits ->
                        // check if the server edit was successful
                        edits.firstOrNull()?.let {
                            if (!it.hasCompletedWithErrors()) {
                                logToUser(false, "Dato agregado")
                            } else {
                                it.error
                            }
                        }
                    }
                } catch (e: ArcGISRuntimeException) {
                    logToUser(true, "Error al agregar los datos" + e.cause?.message)
                }
            }
        }
    }

    private fun selectAttachment() {
        //val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //startActivityForResult(i, RESULT_LOAD_IMAGE)
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        getContent.launch(intent)
    }

    private fun uploadAttachment(result: ActivityResult?) {
        selectedImage = result?.data?.data
        Log.i("DATA: ", selectedImage.toString())

        val imageInputStream: InputStream? = contentResolver.openInputStream(selectedImage!!)
        val imageBytes = bytesFromInputStream(imageInputStream)
        val attachmentName = "Test_" + System.currentTimeMillis() + ".png"

        val addResult: ListenableFuture<Attachment> = mSelectedArcGISFeature!!.addAttachmentAsync(imageBytes, "image/png", attachmentName)

        addResult.addDoneListener {
            val tableResult: ListenableFuture<Void> = mServiceFeatureTable!!.updateFeatureAsync(mSelectedArcGISFeature)
             tableResult.addDoneListener(this::applyEdits)
        }
    }

    /**
     * Converts the given input stream into a byte array.
     *
     * @param inputStream from an image
     * @return an array of bytes from the input stream
     * @throws IOException if input stream can't be read
     */
    @Throws(IOException::class)
    private fun bytesFromInputStream(inputStream: InputStream?): ByteArray {
        ByteArrayOutputStream().use { byteBuffer ->
            val bufferSize = 1024
            val buffer = ByteArray(bufferSize)
            var len: Int = 0
            while (inputStream?.read(buffer).also {
                    if (it != null) {
                        len = it
                    }
                } != -1) {
                byteBuffer.write(buffer, 0, len)
            }
            return byteBuffer.toByteArray()
        }
    }
    fun AppCompatActivity.logToUser(isError: Boolean, message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        if (isError) {
            Log.e(this::class.java.simpleName, message)
        } else {
            Log.d(this::class.java.simpleName, message)
        }
    }
}