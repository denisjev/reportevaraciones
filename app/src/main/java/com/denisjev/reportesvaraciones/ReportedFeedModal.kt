package com.denisjev.reportesvaraciones

import android.graphics.Bitmap
import com.esri.arcgisruntime.data.ArcGISFeature
import com.esri.arcgisruntime.data.Attachment
import java.io.File

class ReportedFeedModal     // creating a constructor class.
    (// creating getter and setter methods.
    // variables for storing our author image,
    // author name, postDate,postDescription,
    // post image,post likes,post comments.
    var authorImage: String = "",
    var authorName: String = "",
    var postDate: String = "",
    var postDescription: String = "",
    var postIV: String = "",
    var postFeature: ArcGISFeature,
    //var postIVImage: Bitmap? = null,
    var postLikes: String = "",
    var postComments: String = ""
)