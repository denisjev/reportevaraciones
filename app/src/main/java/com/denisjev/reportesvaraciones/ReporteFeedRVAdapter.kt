package com.denisjev.reportesvaraciones

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.esri.arcgisruntime.concurrent.ListenableFuture
import com.esri.arcgisruntime.data.ArcGISFeature
import com.esri.arcgisruntime.data.Attachment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.io.InputStream

class ReportedFeedRVAdapter(
    private val reportedFeedModalArrayList: List<ReportedFeedModal>,
    private val context: Context
) : RecyclerView.Adapter<ReportedFeedRVAdapter.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflating our layout for item of recycler view item.
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.element_feed_rv_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modal: ReportedFeedModal = reportedFeedModalArrayList[position]

        // setting data to each view of recyclerview item.
        Picasso.get().load(modal.authorImage).into(holder.authorIV)
        holder.authorNameTV.setText(modal.authorName)
        holder.timeTV.setText(modal.postDate)
        holder.descTV.setText(modal.postDescription)
        holder.postIV.setImageDrawable(null)
        loadImage(modal.postFeature, holder.postIV)

        //Picasso.get().load(modal.postIVImage).into(holder.postIV)
        //holder.likesTV.setText(modal.postLikes)
        //holder.commentsTV.setText(modal.postComments)
    }

    private fun loadImage(postFeature: ArcGISFeature, imageView: ImageView) {
        val attachment: ListenableFuture<List<Attachment>> = postFeature.fetchAttachmentsAsync()

        attachment.addDoneListener {
            val resultAttachment = attachment.get()

            if(resultAttachment.isEmpty())
                return@addDoneListener

            val fetchDataAsync : ListenableFuture<InputStream> = resultAttachment[0].fetchDataAsync()

            fetchDataAsync.addDoneListener{
                try {
                    val `inputStream` = fetchDataAsync.get()
                    var image: Bitmap? = null
                    image = BitmapFactory.decodeStream(`inputStream`)
                    imageView.setImageBitmap(image)
                }
                catch (e: Exception) {
                    Log.e("Error Message", e.message.toString())
                    e.printStackTrace()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        // returning the size of our array list.
        return reportedFeedModalArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // creating variables for our views
        // of recycler view items.
        var authorIV: CircleImageView
        var authorNameTV: TextView
        var timeTV: TextView
        var descTV: TextView
        var postIV: ImageView
        /*var likesTV: TextView
        var commentsTV: TextView
        var shareLL: LinearLayout*/

        init {
            // initializing our variables
            //shareLL = itemView.findViewById(R.id.idLLShare)
            authorIV = itemView.findViewById(R.id.idCVAuthor)
            authorNameTV = itemView.findViewById(R.id.idTVAuthorName)
            timeTV = itemView.findViewById(R.id.idTVTime)
            descTV = itemView.findViewById(R.id.idTVDescription)
            postIV = itemView.findViewById(R.id.idIVPost)
            //likesTV = itemView.findViewById(R.id.idTVLikes)
            //commentsTV = itemView.findViewById(R.id.idTVComments)
        }
    }
}